﻿using System;
namespace game_of_life
{
    public class GridField
    {
        public GridField(String name)
        {
            Name = name;
            Grid = new int[50,50];
        }

        public String Name { get; set; }
        public int[,] Grid { get; set; }

        public void setEmptyGrid()
        {
            for (int x = 0; x < this.Grid.GetLength(0); x++)
            {
                for (int y = 0; y < this.Grid.GetLength(0); y++)
                {
                    this.Grid[x, y] = 0;
                }
            }
        }
        public void togglePointGrid(int x, int y)
        {
            if (this.Grid[x, y] == 0)
            {
                this.Grid[x, y] = 0;
            }
            else
            {
                this.Grid[x, y] = 1;
            }
            
        }
    }
}
